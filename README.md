About
=====

An example showing how to create a spherical world in Unity. 

The player will always stay on top of the sphere, while the sphere rotates to simulate movement.

Movement and rotation is implemented using physics, giving correct response to collisions etc.


License
=======

This software is released under the [UNLICENSE](http://unlicense.org/)
license, see the file UNLICENSE for more information.
