﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Controls the player behaviour.
///
/// The player rotates around its own local axis, while movement
/// is handled by rotating the world around.
///
/// All movement (rotation) is done using physics, which means the
/// player will correctly collide with obstacles etc.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class PlayerBehaviour : MonoBehaviour
{
    #region Inspector Variables
    [Header("Player Properties")]

    [Tooltip("The movement speed of the Player")]
    [SerializeField] private float _moveSpeed = 100f;

    [Tooltip("The turning speed of the Player")]
    [SerializeField] private float _turnSpeed = 100f;


    [Header("References")]
    
    [Tooltip("A reference to the rigidbody of the world")]
    [SerializeField] private Rigidbody _worldRigidbody;
    #endregion // Inspector Variables

    #region Private Variables
    private Transform _transform;
    private Rigidbody m_Rigidbody;
    #endregion // Private Variables

    #region Unity Methods
    void Awake()
    {
        _transform = transform;
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // Handle turning
        float turn = Input.GetAxis("Horizontal");

        Quaternion playerRotation =
            Quaternion.AngleAxis(
                turn * _turnSpeed * Time.deltaTime,
                _transform.up);

        m_Rigidbody.MoveRotation(
            playerRotation * m_Rigidbody.rotation);

        // Handle movement
        float move = Input.GetAxis("Vertical");

        Quaternion sphereRotation =
            Quaternion.AngleAxis(
                move * _moveSpeed * Time.deltaTime,
                -_transform.right);

        _worldRigidbody.MoveRotation(
            sphereRotation * _worldRigidbody.rotation);
    }
    #endregion // Unity Methods
}
